/**
 * Treasure enumeration for Assignment 7
 *
 * @author Matthew Hill
 */

public enum Treasure {
    Wood,
    Statue,
    Coins,
    Food,
    Rags;
}
