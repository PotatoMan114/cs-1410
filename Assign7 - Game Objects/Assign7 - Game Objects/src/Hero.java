/**
 * Hero Class for Assignment 7
 *
 * @author Matthew Hill
 */

import java.util.ArrayList;

public class Hero extends Entity{

    public Hero(String name, int x, int y) {
        super(x, y);
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void attack(Entity entity) {
        if (entity instanceof Dragon) {
            String colorForCoins = "Golden";
            if (((Dragon)entity).getColor().equals(colorForCoins)) {
                System.out.println(this.getName() + " bravely defeated the " + colorForCoins + " dragon and came away with gold coins as a prize.");
                this.getTreasures().add(Treasure.Coins);
            }
            else {
                System.out.println(this.getName() + " bravely defeated the " + ((Dragon)entity).getColor() + " dragon.");
            }
        }
        else if (entity instanceof Crate) {
            System.out.println(this.getName() + " crushed the crate into bits and found " + ((Crate)entity).getTreasure().toString() + ".");
            this.getTreasures().add(((Crate)entity).getTreasure());
        }
    }

    public ArrayList<Treasure> getTreasures() {
        return treasures;
    }

    @Override
    public String toString() {
        String str = name;
        str += " standing at ";
        str += getPosition().toString();
        return str;
    }

    private String name;
    private ArrayList<Treasure> treasures = new ArrayList<>();

}
