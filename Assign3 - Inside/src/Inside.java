import java.lang.StrictMath;
/**
 * Assignment 3 for CS 1410
 * This program determines if points are contained within circles or rectangles.
 *
 * @author Matthew Hill
 */
public class Inside {
    /**
     * This is the primary driver code to test the "inside" capabilities of the
     * various functions.
     *
     * @author Matthew Hill
     */

    public static void main(String[] args) {
        double[] ptX = { 1, 2, 3, 4 };
        double[] ptY = { 1, 2, 3, 4 };
        double[] circleX = { 0, 5 };
        double[] circleY = { 0, 5 };
        double[] circleRadius = { 3, 3 };
        double[] rectLeft = { -2.5, -2.5 };
        double[] rectTop = { 2.5, 5.0 };
        double[] rectWidth = { 6.0, 5.0 };
        double[] rectHeight = { 5.0, 2.5 };


        System.out.println("--- Report of Points and Circles ---");
        System.out.println();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < ptX.length; j++) {
                // j is points, i is shape information
                reportPoint(ptX[j], ptY[j]);
                System.out.print(" is ");
                if (isPointInsideCircle(ptX[j], ptY[j], circleX[i], circleY[i], circleRadius[i])) {
                    System.out.print("inside");
                }
                else {
                    System.out.print("outside");
                }
                System.out.print(" ");
                reportCircle(circleX[i], circleY[i], circleRadius[i]);
                System.out.println();
            }
        }
        System.out.println();
        System.out.println("--- Report of Points and Rectangles");
        System.out.println();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < ptX.length; j++) {
                // j is points, i is shape information
                reportPoint(ptX[j], ptY[j]);
                System.out.print(" is ");
                if (isPointInsideRectangle(ptX[j], ptY[j], rectLeft[i], rectTop[i], rectWidth[i], rectHeight[i])) {
                    System.out.print("inside");
                }
                else {
                    System.out.print("outside");
                }
                System.out.print(" ");
                reportRectangle(rectLeft[i], rectTop[i], rectWidth[i], rectHeight[i]);
                System.out.println();
            }
        }
    }

    static void reportPoint(double x, double y) {
        System.out.print("Point(" + x + ", " + y + ")");
    }

    static void reportCircle(double x, double y, double r) {
        System.out.print("Circle(" + x + ", " + y + ") Radius: " + r);
    }

    static void reportRectangle(double left, double top, double width, double height) {
        double right = left + width;
        double bottom = top - height;
        System.out.print("Rectangle(" + left + ", " + top + ", " + right + ", " + ", " + bottom + ")");
    }

    static boolean isPointInsideCircle(double ptX, double ptY, double circleX, double circleY, double circleRadius) {
        //uses pythagorean theorem to find the distance from (circleX, circleY) to (ptX, ptY)
        double verticalDifference = StrictMath.abs(ptY - circleY);
        double horizontalDifference = StrictMath.abs(ptX - circleX);

        double distance = StrictMath.sqrt(StrictMath.pow(verticalDifference, 2) + StrictMath.pow(horizontalDifference, 2));
        if (distance <= circleRadius) {
            return true;
        }
        else {
            return false;
        }
    }

    static boolean isPointInsideRectangle(double ptX, double ptY, double rLeft, double rTop, double rWidth, double rHeight) {
        double rRight = rLeft + rWidth;
        double rBottom = rTop - rHeight;

        if (ptY <= rTop && ptY >= rBottom && ptX >= rLeft && ptX <= rRight) {
            return true;
        }
        else {
            return false;
        }
    }

}
