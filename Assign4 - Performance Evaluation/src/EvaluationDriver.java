import java.util.Arrays;

/**
 * Assignment 4 for CS 1410
 * This program evaluates the linear and binary searching, along
 * with comparing performance difference between the selection sort
 * and the built-in java.util.Arrays.sort.
 *
 * @author Matthew Hill
 */
public class EvaluationDriver {
    static final int MAX_VALUE = 1_000_000;
    static final int MAX_ARRAY_SIZE = 100_000;
    static final int ARRAY_INCREMENT = 20_000;
    static final int NUMBER_SEARCHES = 50_000;

    public static void main(String[] args) {

        demoLinearSearchUnsorted();
        demoLinearSearchSorted();
        demoBinarySearchSelectionSort();
        demoBinarySearchFastSort();
    }

    /**
     * Demonstrates linear searching over an unsorted array
     *
     * @author Matthew Hill
     */
    public static void demoLinearSearchUnsorted() {
        System.out.println("--- Linear Search Timing (unsorted) ---");
        for (int loopIndex = ARRAY_INCREMENT; loopIndex <= MAX_ARRAY_SIZE; loopIndex+= ARRAY_INCREMENT) {
            int[] data = generateNumbers(ARRAY_INCREMENT, MAX_VALUE);
            if (data == null) {
                System.out.println("Error! Data is a null value!");
                System.out.println(); //prints a blank line.
            }
            else {
                int timesFound = 0;
                long startTime = System.currentTimeMillis();
                for (int j = 0; j < NUMBER_SEARCHES; j++) {
                    int key = (int) (Math.random() * MAX_VALUE);
                    if (linearSearch(data, key)) {
                        timesFound++;
                    }
                }
                long endTime = System.currentTimeMillis();
                long totalTime = endTime - startTime;

                //Report information:
                reportInformation(loopIndex, timesFound, totalTime);
            }

        }
    }

    /**
     * Demonstrates linear searching over a sorted array
     *
     * @author Matthew Hill
     */
    public static void demoLinearSearchSorted() {
        System.out.println("--- Linear Search Timing (Selection Sort) ---");
        for (int loopIndex = ARRAY_INCREMENT; loopIndex <= MAX_ARRAY_SIZE; loopIndex+= ARRAY_INCREMENT) {
            int[] data = generateNumbers(ARRAY_INCREMENT, MAX_VALUE);
            if (data == null) {
                System.out.println("Error! Data is a null value!");
                System.out.println(); //Prints a blank line.
            }
            else {
                int timesFound = 0;
                long startTime = System.currentTimeMillis();
                selectionSort(data);
                for (int j = 0; j < NUMBER_SEARCHES; j++) {
                    int key = (int) (Math.random() * MAX_VALUE);
                    if (linearSearch(data, key)) {
                        timesFound++;
                    }
                }
                long endTime = System.currentTimeMillis();
                long totalTime = endTime - startTime;

                //Report information:
                reportInformation(loopIndex, timesFound, totalTime);
            }

        }
    }

    /**
     * Demonstrates binary searching when using a Selection Sort
     *
     * @author Matthew Hill
     */
    public static void demoBinarySearchSelectionSort() {
        System.out.println("--- Binary Search Timing (Selection Sort) ---");
        for (int loopIndex = ARRAY_INCREMENT; loopIndex <= MAX_ARRAY_SIZE; loopIndex+= ARRAY_INCREMENT) {
            int[] data = generateNumbers(ARRAY_INCREMENT, MAX_VALUE);
            if (data == null) {
                System.out.println("Error! Data is a null value!");
                System.out.println(); //Prints a blank line.
            }
            else {
                int timesFound = 0;
                long startTime = System.currentTimeMillis();
                selectionSort(data);
                for (int j = 0; j < NUMBER_SEARCHES; j++) {
                    int key = (int) (Math.random() * MAX_VALUE);
                    if (binarySearch(data, key)) {
                        timesFound++;
                    }
                }
                long endTime = System.currentTimeMillis();
                long totalTime = endTime - startTime;

                //Report information:
                reportInformation(loopIndex, timesFound, totalTime);
            }

        }
    }

    /**
     * Demonstrates binary searching when using the built in Arrays.sort
     *
     * @author Matthew Hill
     */
    public static void demoBinarySearchFastSort() {
        System.out.println("--- Binary Search Timing (Arrays.sort) ---");
        for (int loopIndex = ARRAY_INCREMENT; loopIndex <= MAX_ARRAY_SIZE; loopIndex+= ARRAY_INCREMENT) {
            int[] data = generateNumbers(ARRAY_INCREMENT, MAX_VALUE);
            if (data == null) {
                System.out.println("Error! Data is a null value!");
                System.out.println(); //Prints a blank line.
            }
            else {
                int timesFound = 0;
                long startTime = System.currentTimeMillis();
                Arrays.sort(data);
                for (int j = 0; j < NUMBER_SEARCHES; j++) {
                    int key = (int) (Math.random() * MAX_VALUE);
                    if (binarySearch(data, key)) {
                        timesFound++;
                    }
                }
                long endTime = System.currentTimeMillis();
                long totalTime = endTime - startTime;

                //Report information:
                reportInformation(loopIndex, timesFound, totalTime);
            }

        }
    }

    /**
     * Reports information about a single search loop
     *
     * @author Matthew Hill
     */
    public static void reportInformation(int numberOfTimes, int timesFound, long searchTime) {
        System.out.printf("%-21s : %d\n", "Number of items", numberOfTimes);
        System.out.printf("%-21s : %d\n", "Times value was found", timesFound);
        System.out.printf("%-21s : %d ms\n", "Total search time", searchTime);
        System.out.println(); //prints a blank line
    }

    /**
     * Randomly generates an integer array
     *
     * @author Matthew Hill
     */
    public static int[] generateNumbers(int howMany, int maxValue) {
        if (howMany > 0) {
            int[] data = new int[howMany];
            for (int i = 0; i < howMany; i++) {
                data[i] = (int)(Math.random() * maxValue);
            }
            return data;
        }
        else {
            return null;
        }
    }

    /**
     * Searches an array of integers using a binary search
     *
     * @author Matthew Hill
     */
    public static boolean linearSearch(int[] data, int search) {
        for (int dataPiece : data) {
            if (dataPiece == search) {
                return true;
            }
        }
        return false;
    }

    /**
     * Searches an array of integers using a binary search
     *
     * @author Matthew Hill
     */
    public static boolean binarySearch(int[] data, int search) {
        int low = 0;
        int high = data.length - 1;
        while (high >= low) {
            int mid = (high + low) / 2;
            if (data[mid] == search) {
                return true;
            }
            else if (search < data[mid]) {
                high = mid - 1;
            }
            else {
                low = mid + 1;
            }
        }
        return false;
    }

    /**
     * Sorts an array of integers using a selection sort, greatest to least
     *
     * @author Matthew Hill
     */
    public static void selectionSort(int[] data) {
        for (int i = 0; i < data.length - 1; i++) {
            int currentMinIndex = i;
            for (int j = i + 1; j < data.length; j++) {
                if (data[j] < data[currentMinIndex]) {
                    currentMinIndex = j;
                }
            }
            if (currentMinIndex != i) {
                int savedInt = data[currentMinIndex];
                data[currentMinIndex] = data[i];
                data[i] = savedInt;
            }
        }
    }
}
