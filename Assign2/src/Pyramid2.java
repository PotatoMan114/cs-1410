import java.util.Scanner;
import java.lang.StrictMath;
public class Pyramid2 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the number of lines: ");
        long lineCount = input.nextLong();
        long maxNumber = (long)StrictMath.pow(2, lineCount - 1); //zero index (2 ^ 0 = 1)
        String maxNumberString = " " + maxNumber; //empty space before each number, so " ".
        for (long line = 1; line <= lineCount; line++) {
            long maxNumPerLine = maxNumber; //Each line has a different maximum number

            for (long column = lineCount - line; column > 0; column--){
                String format = "%" + maxNumberString.length() + "c";
                System.out.printf(format, ' ');
            }
            for (long number = 1; number <= maxNumPerLine; number *= 2) {
                String format = "%" + maxNumberString.length();
                if (number <= (long)StrictMath.pow(2, line - 1)) {
                    format += "d";
                    System.out.printf(format, number);
                }
                else {
                    number /= 2; // counteracts iterator.
                    maxNumPerLine /= 2; //reduces the line's maximum number
                }
            }

            for (long number = maxNumPerLine / 2; number > 0; number /= 2) {
                String format = "%" + maxNumberString.length();
                if (number <= (long)StrictMath.pow(2, line - 1)) {
                    format += "d";
                    System.out.printf(format, number);
                }
                else {
                    format += "c";
                    System.out.printf(format, ' ');
                }
            }
            System.out.print("\n");
        }
    }
}