public class BinarySearchTree<E extends Comparable> {
    private TreeNode<E> root = null;
    private int nodeCount = 0;

    public BinarySearchTree() {

    }

    public boolean insert(E value) {

        if (root == null) {
            TreeNode<E> node = new TreeNode<>(value);
            root = node;
            nodeCount++;
            return true;
        }
        else {
            TreeNode<E> parent = null;
            TreeNode<E> node = root;
            while (node != null) {
                parent = node;
                if (value.compareTo(node.value) < 0) {
                    node = node.left;
                }
                else if (value.compareTo(node.value) > 0) {
                    node = node.right;
                }
                else {
                    //value is duplicate
                    return false;
                }
            }
            TreeNode<E> newNode = new TreeNode<>(value);
            if (value.compareTo(parent.value) < 0) {
                parent.left = newNode;
                nodeCount++;
                return true;
            }

            else {
                parent.right = newNode;
                nodeCount++;
                return true;
            }
        }
    }

    public boolean remove(E value) {

        if (!search(value)) {
            return false; //value is not in the tree
        }

        //find node with value
        TreeNode<E> parent = null;
        TreeNode<E> node = root;

        boolean done = false;
        while (!done) {
            if (node.value.compareTo(value) > 0) {
                parent = node;
                node = node.left;
            } else if (node.value.compareTo(value) < 0) {
                parent = node;
                node = node.right;
            } else {
                done = true;
            }
        }

        if (node.left == null) {
            if (parent == null) {
                root = node.right;
                nodeCount--;
                return true;
            } else {
                if (parent.value.compareTo(value) < 0) {
                    parent.right = node.right;
                    nodeCount--;
                    return true;
                } else {
                    parent.left = node.right;
                    nodeCount--;
                    return true;
                }
            }
        } else {
            TreeNode<E> parentOfRight = node;
            TreeNode<E> rightMost = node.left;
            while (rightMost.right != null) {
                parentOfRight = rightMost;
                rightMost = rightMost.right;
            }
            node.value = rightMost.value;
            if (parentOfRight.right == rightMost) {
                parentOfRight.right = rightMost.left;
                nodeCount--;
                return true;
            } else {
                parentOfRight.left = rightMost.left;
                nodeCount--;
                return true;
            }
        }
    }

    public boolean search (E value) {
        TreeNode<E> node = root;

        while (node != null && node.value.compareTo(value) != 0) {
            if (value.compareTo(node.value) < 0) {
                node = node.left;
            }
            else {
                node = node.right;
            }
        }
        if (node == null) {
            return false;
        }
        return true;
    }

    public void display(String message) {
        System.out.println(message);
        display(root);
    }

    public int numberNodes() {
        return numberNodes(root);
    }

    public int numberLeafNodes() {
        return numberLeafNodes(root);
    }

    public int height() {
        //No nodes: -1
        //One node: 0
        return height(root);
    }

    private void display(TreeNode<E> currentNode) {
        if (currentNode == null) {
            return;
        }
        display(currentNode.left);
        System.out.println(currentNode.value);
        display(currentNode.right);


    }

    private int numberNodes(TreeNode<E> currentNode) {
        if (currentNode != null) {
            return 1 + numberNodes(currentNode.left) + numberNodes(currentNode.right);
        }
        else {
            return 0;
        }
    }

    private int numberLeafNodes(TreeNode<E> currentNode) {
        if (currentNode == null) {
            return 0;
        }
        else if (currentNode.right == null && currentNode.left == null) {
            return 1;
        }
        else {
            return numberLeafNodes(currentNode.right) + numberLeafNodes(currentNode.left);
        }
    }

    private int height(TreeNode<E> currentNode) {
        if (currentNode == null) {
            return -1;
        }
        int heightOfLeft = height(currentNode.left);
        int heightOfRight = height(currentNode.right);
        if (heightOfLeft > heightOfRight) {
            return heightOfLeft + 1;
        }
        else {
            return heightOfRight + 1;
        }
    }

    private class TreeNode<E extends Comparable>{
        public E value;
        public TreeNode<E> left;
        public TreeNode<E> right;

        public TreeNode(E value) {
            this.value = value;
        }

    }
}
