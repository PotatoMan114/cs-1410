public class JulianDate extends Date{
    private static final int DAYS_FROM_1_1_1_TO_1_1_1970 = 719164; //given in assignment description

    public JulianDate() {
        long currentTimeInMilliseconds = System.currentTimeMillis() + java.util.TimeZone.getDefault().getRawOffset();
        int daysToToday = (int)(currentTimeInMilliseconds / getMillisecondsInOneDay());
        addDays(daysToToday + DAYS_FROM_1_1_1_TO_1_1_1970);
    }

    public JulianDate(int year, int month, int day) {
        super(year, month, day);
    }
}
