public class ClassDemo {
    public static void main(String[] args) {

    }
}

class Circle {
    double radius;

    Circle() {
        radius = 1;
    }

    Circle(double tempRadius) {
        radius = tempRadius;
    }

    double getArea() {
        return Math.PI * radius * radius;
    }
}