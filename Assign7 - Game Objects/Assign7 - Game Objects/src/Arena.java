/**
 * Arena Class for Assignment 7
 *
 * @author Matthew Hill
 */

import java.util.ArrayList;
public class Arena {

    public Arena(int x, int y) {
        for (int i = 0; i < x; i++) {
            this.grid.add((new Entity[y]));
        }

    }

    public ArrayList<Entity[]> getGrid() {
        return this.grid;
    }

    public boolean add(Entity entity) {
        if (this.isEmptySpace(entity.getPosition().x, entity.getPosition().y)) {
            if (entity instanceof Hero) {
                if (!this.hasHero()) {
                    this.grid.get(entity.getPosition().x)[entity.getPosition().y] = entity;
                    System.out.println("Successfully added '" + entity.toString() + "' to the game arena.");
                    return true;
                }
                else {
                    System.out.println("Could not add '" + entity.toString() + "' because there is already a hero in the arena.");
                    return false;
                }
            }
            else {
                this.grid.get(entity.getPosition().x)[entity.getPosition().y] = entity;
                System.out.println("Successfully added '" + entity.toString() + "' to the game arena.");

                return true;
            }
        }
        else {
            System.out.println("Could not add '" + entity.toString() + "' because another entity is already there.");
        }
        return false;
    }

    public boolean moveHero(int x, int y) {
        if (!this.hasHero()) {
            return false;
        }
        Hero hero = this.getHero();
        this.grid.get(hero.getPosition().x)[hero.getPosition().y] = null;
        Entity target = this.grid.get(x)[y];
        hero.attack(target);
        this.grid.get(x)[y] = hero;
        this.grid.get(x)[y].getPosition().x = x;
        this.grid.get(x)[y].getPosition().y = y;

        System.out.println(hero.getName() + " moved to " + (new Position(x, y)).toString());
        return true;
    }

    public Hero getHero() {
        for (int x = 0; x < this.grid.size(); x++) {
            for (int y = 0; y < this.grid.get(x).length; y++) {
                if (this.grid.get(x)[y] != null) {
                    if (this.grid.get(x)[y] instanceof Hero) {
                        return (Hero)this.grid.get(x)[y];
                    }
                }
            }
        }
        return null;
    }

    public void reportHero() {
        Hero hero = getHero();
        System.out.printf("--- Hero report for %s ---\n", hero.getName());
        System.out.println("Position: " + hero.getPosition().toString());
        System.out.println("Treasures:");
        for (Treasure treasure : hero.getTreasures()) {
            System.out.println("\t" + treasure.toString());
        }
        System.out.println();
    }

    public int getEntityCount() {
        int count = 0;
        for (int x = 0; x < this.grid.size(); x++) {
            for (int y = 0; y < this.grid.get(x).length; y++) {
                if (this.grid.get(x)[y] != null) {
                    count++;
                }
            }
        }
        return count;
    }

    public int getDragonCount() {
        int count = 0;
        for (int x = 0; x < this.grid.size(); x++) {
            for (int y = 0; y < this.grid.get(x).length; y++) {
                if (this.grid.get(x)[y] instanceof Dragon) {
                    count++;
                }
            }
        }
        return count;
    }

    public int getTreasureCount(Treasure treasure) {
        int count = 0;
        for (int x = 0; x < this.grid.size(); x++) {
            for (int y = 0; y < this.grid.get(x).length; y++) {
                if (this.grid.get(x)[y] instanceof Crate) {
                    if (((Crate) this.grid.get(x)[y]).getTreasure() == treasure) {
                        count++;
                    }
                }
            }
        }
        return count;
    }

    private boolean isEmptySpace(int x, int y) {
        if (this.grid.get(x)[y] == null) {
            return true;
        }
        return false;
    }

    private boolean hasHero() {
        for (int x = 0; x < this.grid.size(); x++) {
            for (int y = 0; y < this.grid.get(x).length; y++) {
                if (this.grid.get(x)[y] != null) {
                    if (this.grid.get(x)[y] instanceof Hero) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private ArrayList<Entity[]> grid = new ArrayList<>();

}
