public class Date {
    public static final int MILLISECONDS_IN_ONE_DAY = 24 * 60 * 60 * 1000;
    private int year;
    private int month;
    private int day;

    public Date() {
        this.year = 1;
        this.month = 1;
        this.day = 1;
    }

    public Date(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public void addDays(int days) {
        for (int i = 0; i < days; i++) {
            this.day++;
            if (this.day > this.getNumberOfDaysInMonth(this.year, this.month)) {
                this.day = 1;
                this.month++;
                if (this.month > 12) {
                    this.month = 1;
                    this.year++;
                }
            }
        }
    }
    public void subtractDays(int days) {
        for (int i = 0; i < days; i++) {
            this.day--;
            if (this.day < 1) {
                this.month--;
                if (month < 1) {
                    this.month = 12;
                    this.year--;
                }
                this.day = this.getNumberOfDaysInMonth(this.year, this.month);
            }
        }
    }

    public boolean isLeapYear() {
        return this.isLeapYear(this.year);
    }

    public boolean isLeapYear(int year) {
        if (this instanceof JulianDate) {
            return year % 4 == 0;

        }
        else if (this instanceof GregorianDate) {
            return (year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0);
        }
        else {
            return false;
        }
    }

    public void printShortDate() {
        System.out.print("" + this.month + "/" + this.day + "/" + this.year);
    }

    public void printLongDate() {
        System.out.print("" + getMonthName(this.month) + " " + this.day + ", " + this.year);
    }

    public String getCurrentMonthName() {
        return this.getMonthName(this.month);
    }

    public int getCurrentMonth() {
        return this.month;
    }

    public int getCurrentYear() {
        return this.year;
    }

    public int getCurrentDayOfMonth() {
        return this.day;
    }

    public static int getMillisecondsInOneDay() {
        return MILLISECONDS_IN_ONE_DAY;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setDay(int day) {
        this.day = day;
    }

    private int getNumberOfDaysInMonth(int year, int month) {
        switch (month) {
            case 1: return 31;
            case 2:
                if (isLeapYear(year)) {
                    return 29;
                }
                else {
                    return 28;
                }
            case 3: return 31;
            case 4: return 30;
            case 5: return 31;
            case 6: return 30;
            case 7: return 31;
            case 8: return 31;
            case 9: return 30;
            case 10: return 31;
            case 11: return 30;
            case 12: return 31;
            default: return -1;
        }
    }

    private int getNumberOfDaysInYear(int year) {
        if (isLeapYear(year)) {
            return 366;
        }
        else {
            return 365;
        }
    }

    private String getMonthName(int month) {
        switch (month) {
            case 1: return "January";
            case 2: return "February";
            case 3: return "March";
            case 4: return "April";
            case 5: return "May";
            case 6: return "June";
            case 7: return "July";
            case 8: return "August";
            case 9: return "September";
            case 10: return "October";
            case 11: return "November";
            case 12: return "December";
            default: return "Error";
        }
    }

}
