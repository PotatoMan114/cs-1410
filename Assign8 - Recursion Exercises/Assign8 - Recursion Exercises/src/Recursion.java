public class Recursion {
    public static void main(String[] args) {

        int[] sumMe = { 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89 };
        System.out.printf("Array Sum: %d\n", arraySum(sumMe, 0));

        int[] minMe = { 0, 1, 1, 2, 3, 5, 8, -42, 13, 21, 34, 55, 89 };
        System.out.printf("Array Min: %d\n", arrayMin(minMe, 0));

        String[] amISymmetric =  {
                "You can cage a swallow can't you but you can't swallow a cage can you",
                "I still say cS 1410 is my favorite class"
        };
        for (String test : amISymmetric) {
            String[] words = test.toLowerCase().split(" "); //Split and lowerCase already done.
            System.out.println();
            System.out.println(test);
            System.out.printf("Is word symmetric: %b\n", isWordSymmetric(words, 0, words.length - 1));
        }

        double weights[][] = {
                { 51.18 },
                { 55.90, 131.25 },
                { 69.05, 133.66, 132.82 },
                { 53.43, 139.61, 134.06, 121.63 }
        };
        //                  { 51.18 },
        //              { 55.90, 131.25 },
        //          { 69.05, 133.66, 132.82 },
        //      { 53.43, 139.61, 134.06, 121.63 }
        System.out.println();
        System.out.println("--- Weight Pyramid ---");
        for (int row = 0; row < weights.length; row++) {
            for (int column = 0; column < weights[row].length; column++) {
                double weight = computePyramidWeights(weights, row, column);
                System.out.printf("%.2f ", weight);
            }
            System.out.println();
        }

        char image[][] = {
                {'*','*',' ',' ',' ',' ',' ',' ','*',' '},
                {' ','*',' ',' ',' ',' ',' ',' ','*',' '},
                {' ',' ',' ',' ',' ',' ','*','*',' ',' '},
                {' ','*',' ',' ','*','*','*',' ',' ',' '},
                {' ','*','*',' ','*',' ','*',' ','*',' '},
                {' ','*','*',' ','*','*','*','*','*','*'},
                {' ',' ',' ',' ',' ',' ',' ',' ','*',' '},
                {' ',' ',' ',' ',' ',' ',' ',' ','*',' '},
                {' ',' ',' ','*','*','*',' ',' ','*',' '},
                {' ',' ',' ',' ',' ','*',' ',' ','*',' '}
        };
        int howMany = howManyOrganisms(image);
        System.out.println();
        System.out.println("--- Labeled Organism Image ---");
        for (char[] line : image) {
            for (char item : line) {
                System.out.print(item);
            }
            System.out.println();
        }
        System.out.printf("There are %d organisms in the image.\n", howMany);

    }

    public static boolean isWordSymmetric(String[] words, int start, int end) {
        boolean valid;
        if (start >= end) {
            return true;
        }
        if (words[start].toLowerCase().equals(words[end].toLowerCase())) {
            valid = true;
        }
        else {
            valid = false;

        }
        return valid && isWordSymmetric(words, start + 1, end - 1);
    }

    public static long arraySum(int[] data, int position) {
        if (position >= data.length) {
            return 0;
        }
        else {
            int dataPiece = data[position];
            return dataPiece + arraySum(data, position + 1);
        }
    }

    public static int arrayMin(int[] data, int position) {
        if (position >= data.length) {
            return data[position - 1];
        }
        int dataPiece = data[position];
        if (dataPiece < arrayMin(data, position + 1)) {
            return dataPiece;
        }
        else {
            return arrayMin(data, position + 1);
        }
    }

    public static double computePyramidWeights(double[][] weights, int row, int column) {
        //up-right: row - 1, column
        //up-left: row - 1, column - 1
        if (row < 0 || row >= weights.length) {
            return 0;
        }
        if (column < 0 || column >= weights[row].length) {
            return 0;
        }
        return weights[row][column] +
                (0.5 * computePyramidWeights(weights, row - 1, column - 1)) +
                (0.5 * computePyramidWeights(weights, row - 1, column));
    }

    public static int howManyOrganisms(char[][] image) {
        //iterate through image
        //when an * is encountered, increment count by 1
            //call recursive howManyOrganisms with position
            //recursive howManyOrganisms checks up one, right one, left one, and down one. NOT DIAGONALS
                //When another asterisk is encountered, it goes into another recursive howManyOrganisms with that position.
        int count = 0;
        char label = 'a';
        for (int row = 0; row < image.length; row++) {
            for (int column = 0; column < image[row].length; column++) {
                if (image[row][column] == '*') {
                    labelOrganismImage(image, row, column, label);
                    count++;
                    label++;
                }
            }
        }

        return count;

    }

    public static void labelOrganismImage(char[][] image, int row, int column, char label) {
        image[row][column] = label;
        //check up
        if (isOnImage(image, row - 1, column)) {
            if (image[row - 1][column] == '*') {
                labelOrganismImage(image, row - 1, column, label);
            }
        }

        //check down
        if (isOnImage(image, row + 1, column)) {
            if (image[row + 1][column] == '*') {
                labelOrganismImage(image, row + 1, column, label);
            }
        }

        //check right
        if (isOnImage(image, row, column + 1)) {
            if (image[row][column + 1] == '*') {
                labelOrganismImage(image, row, column + 1, label);
            }
        }

        //check left
        if (isOnImage(image, row, column - 1)) {
            if (image[row][column - 1] == '*') {
                labelOrganismImage(image, row, column - 1, label);
            }
        }
    }

    public static boolean isOnImage(char[][] image, int row, int column) {
        if (row < 0 || row >= image.length) {
            return false;
        }
        if (column < 0 || column >= image[row].length) {
            return false;
        }
        return true;
    }

}
