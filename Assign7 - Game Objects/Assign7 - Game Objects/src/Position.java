/**
 * Position Class for Assignment 7
 *
 * @author Matthew Hill
 */

public class Position {
    public int x;
    public int y;

    public Position() {
        this.x = 0;
        this.y = 0;
    }

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        String str = "(";
        str += Integer.toString(x);
        str += ", ";
        str += Integer.toString(y);
        str += ")";
        return str;
    }
}
