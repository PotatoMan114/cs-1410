/**
 * Entity Class for Assignment 7
 *
 * @author Matthew Hill
 */

public class Entity {

    public Entity(int x, int y) {
        this.position.x = x;
        this.position.y = y;
    }

    public Position getPosition() {
        return position;
    }

    @Override
    public String toString() {
        String str = "Entity at ";
        str += position.toString();
        return str;
    }

    public void setPosition(int x, int y) {
        this.position = new Position(x, y);
    }

    private Position position = new Position();

}
