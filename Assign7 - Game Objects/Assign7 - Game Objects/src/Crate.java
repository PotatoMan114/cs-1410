/**
 * Crate Class for Assignment 7
 *
 * @author Matthew Hill
 */

public class Crate extends Entity{

    public Crate(Treasure treasure, int x, int y) {
        super(x, y);
        this.treasure = treasure;
    }

    public Treasure getTreasure() {
        return this.treasure;
    }

    @Override
    public String toString() {
        String str = "Crate with ";
        str += treasure.toString();
        str += " at ";
        str += getPosition().toString();
        return str;
    }

    private Treasure treasure;

}
