import java.util.*;

public class ISBN {

    //Returns a^b, truncating any decimal points.
    public static int integerPower(int a, int b)
    {
        int answer = a;
        for (int i = 1; i < b; i++)
        {
            answer *= a;
        }
        if (b == 0)
        {
            return 1;
        }
        return answer;
    }

    //Returns the ISBN-10 checksum given the first nine numbers using integer manipulation, not strings or arrays.
    public static int computeChecksum(int isbn9) {

        int checksumSum = 0;
        int digit = 0;
        for (int i = 8; i >= 0; i--) {
            digit++;
            int num = isbn9 / (integerPower(10, i));
            isbn9 -= num * (integerPower(10, i));
            checksumSum += (num * digit);
        }

        return checksumSum % 11;
    }

    public static void main(String[] args) {
        //Input
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the first 9 digits of an ISBN: ");
        int isbn = input.nextInt();

        //Output
        int checksum = computeChecksum(isbn);
        System.out.print("The ISBN-10 number is: ");
        for (int i = 8; i >= 0; i--) {
            int num = isbn / (integerPower(10, i));
            isbn -= num * (integerPower(10, i));
            System.out.print(num);
        }
        if (checksum == 10) {
            System.out.println("X");
        } else {
            System.out.println(checksum);
        }
    }
}