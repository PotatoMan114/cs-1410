import java.util.Scanner;

public class Pyramid1 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.print("Enter the number of lines: ");
        long lineCount = input.nextLong();
        String maxNumber = " " + lineCount; //empty space before each number, so " ".

        for (long line = 1; line <= lineCount; line++) {

            for (long number = lineCount; number > 0; number--) {
                String format = "%" + maxNumber.length();
                if (number <= line) {
                    format += "d";
                    System.out.printf(format, number);
                }
                else {
                    format += "c";
                    System.out.printf(format, ' ');
                }
            }

            for (long number = 2; number <= lineCount; number++) { // long number = 2 to avoid repeating 1
                String format = "%" + maxNumber.length();
                if (number <= line) {
                    format += "d";
                    System.out.printf(format, number);
                }
                else {
                    format += "c";
                    System.out.printf(format, ' ');
                }
            }
            System.out.print("\n"); //new line
        }
    }
}