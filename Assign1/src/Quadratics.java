import java.util.*;
import java.lang.StrictMath;

public class Quadratics {

    //Returns the result of the discriminant of the quadratic formula.
    public static double quadraticDiscriminant(double a, double b, double c) {
        return (b * b) - (4 * a * c);
    }

    // Returns the number of roots a quadratic equation has based on a, b, and c.
    public static int rootAmount(double a, double b, double c) {

        if (quadraticDiscriminant(a, b, c) > 0) {
            return 2;
        }
        else if (quadraticDiscriminant(a, b, c) == 0) {
            return 1;
        }
        else {
            return 0;
        }
    }
    // This method returns the right (or only) root of a quadratic equation given a, b, and c. Adds by the square root.
    // This is the method used when rootAmount() returns 1 or 2.
    public static double positiveRoot(double a, double b, double c) {
        return (((-b) + StrictMath.sqrt(quadraticDiscriminant(a, b, c))) / (2 * a));
    }

    // This method returns the left root of a quadratic equation given a, b, and c. Subtracts by the square root.
    // This is the method used when rootAmount() returns 2.
    public static double negativeRoot(double a, double b, double c) {
        return (((-b) - StrictMath.sqrt(quadraticDiscriminant(a, b, c))) / (2 * a));
    }

    public static void main(String[] args) {
        // User input
        Scanner input = new Scanner(System.in);

        System.out.print("Enter a, b, c: ");
        double a = input.nextDouble();
        double b = input.nextDouble();
        double c = input.nextDouble();

        // Results
        if (rootAmount(a, b, c) == 2) {
            System.out.println("There are two roots for the quadratic equation with these coefficients.");
            System.out.println("r1 = " + positiveRoot(a, b, c));
            System.out.println("r2 = " + negativeRoot(a, b, c));
        }
        else if (rootAmount(a, b, c) == 1) {
            System.out.println("There is one root for the quadratic equation with these coefficients.");
            System.out.println("r1 = " + positiveRoot(a, b, c));
        }
        else {
            System.out.println("There are no roots for the quadratic equation with these coefficients.\n");
        }
    }
}