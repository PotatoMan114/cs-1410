/**
 * Dragon Class for Assignment 7
 *
 * @author Matthew Hill
 */

public class Dragon extends Entity{

    public Dragon(String color, int x, int y) {
        super(x, y);
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    @Override
    public String toString() {
        String str = "The ";
        str += color;
        str += " dragon breathing fire at ";
        str += getPosition().toString();
        return str;
    }

    private String color;

}
