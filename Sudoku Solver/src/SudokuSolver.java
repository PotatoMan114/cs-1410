
public class SudokuSolver {

    public static void main(String[] args) {
        //board[row][column]
        solveBoard(getBoard());
    }


    public static void solveBoard(int[][] board) {
        //find notes, plug in big numbers, reduce notes, repeat.

    }

    public static int[][] getBoard() {
        return null;
    }

    //return type?
    public static void findNotes(int[][] board) {

    }

    public static int[] findOneSquareNotes(int[][] board, int x, int y)

    public static boolean checkWin(int[][] board) {
        boolean win = true;
        win = win && checkRows(board);
        win = win && checkColumns(board);
        win = win && checkGroups(board);
        return win;
    }

    public static boolean allNumbersFound(boolean[] numbersFound) {
//        boolean[] numbersFound = makeBooleanArray(data);
        boolean allFound = true;
        for (boolean element : numbersFound) {
            allFound = allFound && element;
        }
        return allFound;
    }

    public static boolean[] makeBooleanArray(int[] data) {
        boolean[] numbersFound = new boolean[9];
        for (int number : data) {
            numbersFound[number - 1] = true;
        }
        return numbersFound;
    }

    public static boolean checkRows(int[][] board) {
        boolean valid = true;
        for (int[] row : board) {
            boolean[] numbersFound = makeBooleanArray(row);
            valid = valid && allNumbersFound(numbersFound);
        }
        return valid;
    }

    public static boolean checkColumns(int[][] board) {
        boolean valid = true;
        for (int columnNumber = 0; columnNumber < 9; columnNumber++) {
            int[] column = makeColumnArray(board, columnNumber);
            boolean[] numbersFound = makeBooleanArray(column);
            valid = valid && allNumbersFound(numbersFound);
        }
        return valid;
    }

    public static int[] makeColumnArray(int[][] board, int columnNumber) {
        int[] column = new int[9];
        for (int row = 0; row < 9; row++) {
            column[row] = board[row][columnNumber];
        }
        return column;
    }

    public static boolean checkGroups(int[][] board) {
        boolean allGroupsValid = true;
        for (int group = 0; group < 9; group++) {
            boolean[] values = new boolean[9];
            int rowStart = (group / 3) * 3;
            int colStart = (group % 3) * 3;

            values[board[rowStart + 0][colStart + 0] - 1] = true;
            values[board[rowStart + 1][colStart + 0] - 1] = true;
            values[board[rowStart + 2][colStart + 0] - 1] = true;

            values[board[rowStart + 0][colStart + 1] - 1] = true;
            values[board[rowStart + 1][colStart + 1] - 1] = true;
            values[board[rowStart + 2][colStart + 1] - 1] = true;

            values[board[rowStart + 0][colStart + 2] - 1] = true;
            values[board[rowStart + 1][colStart + 2] - 1] = true;
            values[board[rowStart + 2][colStart + 2] - 1] = true;

            allGroupsValid = allGroupsValid && allNumbersFound(values);
        }
        return allGroupsValid;

    }

}
