public class GregorianDate extends Date {

    public GregorianDate() {
        long currentTimeInMilliseconds = System.currentTimeMillis() + java.util.TimeZone.getDefault().getRawOffset();
        this.setYear(1970);
        int daysToToday = (int) (currentTimeInMilliseconds / MILLISECONDS_IN_ONE_DAY);
        addDays(daysToToday);
    }

    public GregorianDate(int year, int month, int day) {
        super(year, month, day);
    }
}
